# Super Mare

Ideja je da napravimo igricu slicnu igrici Super Mario, s tim sto bismo je, naravno prilagodili sadrzaju kursa. Igrica bi bila u vise niova, postojale bi opcije sa singleplayer i multiplayer. Tačnije dva igrača koja se trkaju. Osim igraca, u igrici bi postojali "prijatelji" i "neprijatelji". Igrac bi imao svoje karakteristike (npr. brzinu, energiju i slicno) koje se menjaju u odnosu na to koliko je vremena proslo i koliko je i kojih "prijatelja" i "neprijatelja" igrac stekao. 
Ovo bi ukratko bila potencijalna tema naseg projekta.

<ul>
    <li><a href="https://gitlab.com/andjelastajic8">Anđela Stajić 147/2018</a></li>
    <li><a href="https://gitlab.com/milican351">Milica Nikolić 89/2018</a></li>
    <li><a href="https://gitlab.com/lidijadjalovic">Lidija Đalović 107/2018</a></li>
    <li><a href="https://gitlab.com/maksimovicj">Jelena Maksimović 193/2018</a></li>
    <li><a href="https://gitlab.com/mich2702">Marko Vićentijević 123/2016</a></li>
</ul>
